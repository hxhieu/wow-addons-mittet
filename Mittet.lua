-- consts
ModKeys = {
    none = 0,
    alt = 1,
    ctrl = 2,
    shift = 3
}

local addonName, addonNamespace = ...

-- vars
local UPDATE_INTERVAL = 0.1
local timeSinceLastUpdate = 0
local enabled, multiTarget, inCombat = false, false, false

SavedVars = nil
GlobalVars = nil

-- parent container 
local container = CreateFrame("Frame", "Mittet_Container", UIParent)
container:SetSize(256, 15)
container:SetPoint("TOPLEFT", 0, 0)

local containerTexture = container:CreateTexture()
container.background = containerTexture
containerTexture:SetAllPoints()
containerTexture:SetColorTexture(0, 0, 0, 1)

-- color block
local block = CreateFrame("Frame", "Mittet_Color", container)
block:SetSize(256, 3)
block:SetPoint("TOPLEFT", 0, 0)
block.text = block:CreateFontString(nil, "FULLSCREEN_DIALOG", "GameNormalNumberFont");
block.text:SetTextColor(1, 1, 1, 1)
block.text:SetAllPoints();

local blockTexture = block:CreateTexture()
block.background = blockTexture
blockTexture:SetAllPoints()

-- enable text block
local txtEnabled = CreateFrame("Frame", "Mittet_Enabled", container)
txtEnabled:SetSize(48, 12)
txtEnabled:SetPoint("TOPLEFT", 0, -3)
txtEnabled.text = txtEnabled:CreateFontString(nil, "FULLSCREEN_DIALOG", "GameNormalNumberFont");
txtEnabled.text:SetTextColor(1, 1, 1, 1)
txtEnabled.text:SetAllPoints();
txtEnabled.text:SetText("enabled")

local txtEnabledTexture = txtEnabled:CreateTexture()
txtEnabled.background = txtEnabledTexture
txtEnabledTexture:SetAllPoints()
txtEnabledTexture:SetColorTexture(0, 0.5, 0, 1)

-- in combat text block
local txtInCombat = CreateFrame("Frame", "Mittet_InCombat", container)
txtInCombat:SetSize(24, 12)
txtInCombat:SetPoint("TOPLEFT", 48, -3)
txtInCombat.text = txtInCombat:CreateFontString(nil, "FULLSCREEN_DIALOG", "GameNormalNumberFont");
txtInCombat.text:SetTextColor(1, 1, 1, 1)
txtInCombat.text:SetAllPoints();
txtInCombat.text:SetText("ooc")

local txtInCombatTexture = txtInCombat:CreateTexture()
txtInCombat.background = txtInCombatTexture
txtInCombatTexture:SetAllPoints()
txtInCombatTexture:SetColorTexture(0.5, 0, 0, 1)

-- status text block
local txtStatus = CreateFrame("Frame", "Mittet_Status", container)
txtStatus:SetSize(184, 12)
txtStatus:SetPoint("TOPLEFT", 72, -3)
txtStatus.text = txtStatus:CreateFontString(nil, "FULLSCREEN_DIALOG", "GameNormalNumberFont");
txtStatus.text:SetTextColor(1, 1, 1, 1)
txtStatus.text:SetAllPoints();

local txtStatusTexture = txtStatus:CreateTexture()
txtStatus.background = txtStatusTexture
txtStatusTexture:SetAllPoints()
txtStatusTexture:SetColorTexture(0, 0, 0, 1)

-- Overlays

local overlayMulti = CreateFrame("Frame");
overlayMulti:ClearAllPoints();
overlayMulti:SetHeight(40);
overlayMulti:SetWidth(300);
overlayMulti.text = overlayMulti:CreateFontString(nil, "FULLSCREEN_DIALOG", "PVPInfoTextFont");
overlayMulti.text:SetAllPoints();
overlayMulti:SetPoint("CENTER", 0, 200);
overlayMulti.text:SetText("*** MULTI ***")
overlayMulti:Hide()

-- frame event listeners
container:SetScript("OnEvent", function(self, event, ...) self[event](self, ...) end)

-- frame events
container:RegisterEvent("ADDON_LOADED")
container:RegisterEvent("PLAYER_LOGIN")
container:RegisterEvent("PLAYER_LOGOUT")
container:RegisterEvent("PLAYER_SPECIALIZATION_CHANGED")

function block:UPDATE()

    if UnitAffectingCombat('player') then
        if not inCombat then
            inCombat = true
            txtInCombatTexture:SetColorTexture(0, 0.5, 0, 1)
            txtInCombat.text:SetText("ic")
        end
    else
        if inCombat then
            inCombat = false
            txtInCombatTexture:SetColorTexture(0.5, 0, 0, 1)
            txtInCombat.text:SetText("ooc")
            ResetKey()
        end

        -- do nothing when out of combat
        return
    end

    RunAI(multiTarget and FireMode.multi or FireMode.single)
end

-- frame event handlers
function container:ADDON_LOADED(event)
    -- ignore other
    if (event ~= addonName) then return end

    -- load saved vars
    LoadVars()

    -- only register UPDATE if class spells set up
    if SpellDict[SavedVars.class] ~= nil and SpellDict[SavedVars.class][SavedVars.spec] ~= nil then
        -- register update
        block:SetScript("OnUpdate", function(self, deltaTime, ...)
            -- update interval
            timeSinceLastUpdate = timeSinceLastUpdate + deltaTime
            while (timeSinceLastUpdate > UPDATE_INTERVAL) do
                self["UPDATE"]()
                timeSinceLastUpdate = timeSinceLastUpdate - UPDATE_INTERVAL;
            end
        end )
    end
end

function container:PLAYER_LOGIN(event)
    Constructor()
end

function container:PLAYER_LOGOUT(event)
    Destructor()
end

function container:PLAYER_SPECIALIZATION_CHANGED(event)
    -- ignore other
    if (event ~= "player") then return end
    MittetConfig.HideConfigFrame()
    UpdateClassAndSpec()
end

function Constructor()
    -- get class/spec
    UpdateClassAndSpec()

    -- init key
    ResetKey()
end

function Destructor()
    SaveVars()
end

-- load saved vars
function LoadVars()

    if MittetSaved ~= nil then
        SavedVars = MittetSaved
    else
        SavedVars = { }
    end

    if MittetGlobalSaved ~= nil then
        GlobalVars = MittetGlobalSaved
    else
        GlobalVars = { }
    end

    -- init enable
    SetEnable(SavedVars.enabled ~= nil and SavedVars.enabled or false)
end

function SaveVars()
    SavedVars.enabled = enabled

    -- save saved vars
    MittetSaved = SavedVars
    MittetGlobalSaved = GlobalVars
end

-- Custom block color
function SetKey(num, mod)
    local modName = ""

    if mod == ModKeys.alt then
        modName = "ALT+"
    elseif mod == ModKeys.ctrl then
        modName = "CTRL+"
    elseif mod == ModKeys.shift then
        modName = "SHIFT+"
    end

    -- blue channel not using atm
    blockTexture:SetColorTexture(num / 255, mod / 255, 0, 1)
end

function ResetKey()
    SetKey(0, ModKeys.none)
    SetStatusText(nil)
end

function SetStatusText(msg)
    if msg == nil then return end
    txtStatus.text:SetText(msg)
end

function UpdateClassAndSpec()
    if SavedVars == nil then SavedVars = { } end
    id, name, description, icon, background, role, class = GetSpecializationInfoByID(GetSpecializationInfo(GetSpecialization()))
    SavedVars["class"] = strupper(UnitClass("player"))
    SavedVars["spec"] = name
end

function ToggleEnable()
    -- toggle
    SetEnable(not enabled)
end

function SetEnable(toggle)
    if toggle then
        txtEnabled.text:SetText("enabled")
        txtEnabledTexture:SetColorTexture(0, 0.5, 0, 1)
        block:Show()
    else
        txtEnabled.text:SetText("disabled")
        txtEnabledTexture:SetColorTexture(0.5, 0, 0, 1)
        block:Hide()
    end

    enabled = toggle
    ResetKey()
end

function ToggleSingleMulti()
    multiTarget = not multiTarget
    if multiTarget then
        overlayMulti:Show()
    else
        overlayMulti:Hide()
    end
    ResetKey()
end
