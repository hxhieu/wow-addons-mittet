## Interface: 70100
## Title: Mittet
## Author: Hugh Hoang
## Version: 0.1.0
## SavedVariables: MittetGlobalSaved
## SavedVariablesPerCharacter: MittetSaved

libs\LibStub\LibStub.lua
libs\AceGUI-3.0\AceGUI-3.0.xml
libs\LibRangeCheck-2.0\LibRangeCheck-2.0.lua

Mittet_Helpers.lua
SpellDict\include.xml

Mittet_Config.lua
Mittet_GenericAI.lua

Mittet.lua

Mittet_SlashCmd.lua

#Mittet_MainFrame.xml