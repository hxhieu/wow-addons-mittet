local addonName, addonNamespace = ...
local eventHandlers = CreateFrame("Frame")

function Mittet_OnEvent(frame, event, addon, ...)
  if (addon ~= addonName) then return end
    eventHandlers[event]()
end

function Mittet_OnLoad(frame, ...)
  frame:RegisterEvent("ADDON_LOADED")
  frame:RegisterEvent("PLAYER_LOGIN")
  frame:RegisterEvent("PLAYER_LOGOUT")
  frame:RegisterEvent("PLAYER_SPECIALIZATION_CHANGED")
end

function eventHandlers:ADDON_LOADED()
  print("ADDON_LOADED")
end

function eventHandlers:PLAYER_LOGIN()
  print("PLAYER_LOGIN")
end

function eventHandlers:PLAYER_LOGOUT()
  print("PLAYER_LOGOUT")
end

function eventHandlers:PLAYER_SPECIALIZATION_CHANGED()
  print("PLAYER_SPECIALIZATION_CHANGED")
end