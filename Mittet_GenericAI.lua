FireMode = {
    single = 0,
    multi = 1,
    both = 2
}

local rc = LibStub("LibRangeCheck-2.0")

function RunAI(fireMode)

    -- loop them!
    for k, v in pairs(SpellDict[SavedVars.class][SavedVars.spec]) do
        if Cast(v, fireMode) then return end
    end

    -- Nothing to do
    ResetKey()
end

function SpellCooldown(id)
    local start, duration, enabled = GetSpellCooldown(id);
    return duration > 0 and(start or 0) > 0
end

function GetPowerType()
    local powerType = nil
    if SavedVars.class == "HUNTER" then
        powerType = 2
    elseif SavedVars.class == "WARRIOR" then
        powerType = 1
    elseif SavedVars.class == "MAGE" then
        powerType = 0
    elseif SavedVars.class == "ROGUE" then
        powerType = 3
    elseif SavedVars.class == "DEATH KNIGHT" then
        powerType = 6
	elseif SavedVars.class == "SHAMAN" then
        powerType = 11
    end

    return powerType
end

function GetPowerPointType()
    local powerType = nil

    -- combo points
    if SavedVars.class == "ROGUE" then
        powerType = 4
    elseif SavedVars.class == "DEATH KNIGHT" then
        powerType = 5
    elseif SavedVars.class == "PALADIN" then
        powerType = 9
	elseif SavedVars.class == "SHAMAN" then
        powerType = 11
    end

    return powerType
end

function GetCurrentPower()
    return UnitPower("player", GetPowerType())
end

function GetMaxPower()
    return UnitPowerMax("player", GetPowerType())
end

function GetCurrentPowerPoint()
    return UnitPower("player", GetPowerPointType())
end

function GetSpellActive(spellName)
    return SavedVars.Configuration ~= nil
    and SavedVars.Configuration[SavedVars.spec] ~= nil
    and SavedVars.Configuration[SavedVars.spec][spellName] ~= nil
    and SavedVars.Configuration[SavedVars.spec][spellName].active
end

function GetSpellModKey(spellName)
    return SavedVars.Configuration ~= nil
    and SavedVars.Configuration[SavedVars.spec] ~= nil
    and SavedVars.Configuration[SavedVars.spec][spellName] ~= nil
    and SavedVars.Configuration[SavedVars.spec][spellName].mod or 0
end

function GetSpellNumKey(spellName)
    return SavedVars.Configuration ~= nil
    and SavedVars.Configuration[SavedVars.spec] ~= nil
    and SavedVars.Configuration[SavedVars.spec][spellName] ~= nil
    and SavedVars.Configuration[SavedVars.spec][spellName].key or 0
end

function IsInterruptable()
    local name, subText, text, texture, startTime, endTime, isTradeSkill, castID, notInterruptible = UnitCastingInfo("target")
    if name == nil then
        name, subText, text, texture, startTime, endTime, isTradeSkill, castID, notInterruptible = UnitChannelInfo("target")
    end
    return name ~= nil and not notInterruptible
end

function HasBuff(buff, stackCount, remain)
    name, rank, icon, count, dispelType, duration, expires, caster, isStealable, shouldConsolidate, spellID, canApplyAura, isBossDebuff, value1, value2, value3 = UnitBuff("player", buff)
    return spellID ~= nil and count >= stackCount and expires - GetTime() >= remain
end

function PetHasBuff(buff, stackCount)
    name, rank, icon, count, dispelType, duration, expires, caster, isStealable, shouldConsolidate, spellID, canApplyAura, isBossDebuff, value1, value2, value3 = UnitBuff("pet", buff)
    return spellID ~= nil and count >= stackCount
end

function TargetDebuff(buff, selfOnly, stackCount, remain)
	name, rank, icon, count, debuffType, duration, expirationTime, unitCaster, isStealable, shouldConsolidate, spellID = UnitDebuff("target", buff)
	if spellID == nil then 
		return false 
	end
	
	if selfOnly and unitCaster ~= "player" then
		return false
	end
	
	return count >= stackCount and expirationTime - GetTime() >= remain
end

function IsCasting(unit)
	if UnitChannelInfo(unit) ~= nil then return true end
	if UnitCastingInfo(unit) ~= nil then return true end
	
	return false
end

function Cast(spell, mode)
    -- fire mode check
    local fireMode = spell.FireMode or FireMode.single;
    if (mode == FireMode.single and fireMode == FireMode.multi) or(mode == FireMode.multi and fireMode == FireMode.single) then return false end

    -- not meet cast conditions
	
	-- not mount
    if IsMounted() then return false end
	if HasBuff("Ghost Wolf", 0, 0) then return false end
	-- config active
    
	if not GetSpellActive(spell.Name) then return false end
    -- has valid target
    if not UnitIsVisible("target") then return false end
    -- not on cd
    if SpellCooldown(spell.Id) then return false end
    -- enough power
    if GetCurrentPower() < (spell.Power or 0) then return false end
    -- enough power points
    if GetCurrentPowerPoint() < (spell.PowerPoint or 0) then return false end
    -- min health
    if (UnitHealth("player") / UnitHealthMax("player") < (spell.MinHP or 0)) then return false end
	-- max health
	if (UnitHealth("player") / UnitHealthMax("player") > (spell.MaxHP or 1)) then return false end
	-- min target health
    if (UnitHealth("target") / UnitHealthMax("target") < (spell.TargetPct or 0)) then return false end
    -- not casting
    if IsCasting("player") then return false end
	
	local currentCharges, maxCharges, cooldownStart, cooldownDuration = GetSpellCharges(spell.Name)
	if (currentCharges or 1000) < (spell.Charge or 1) then return false end
	
    -- range check
    -- get spell range
	local rangeCheck = spell.Range
	-- or default class/spec range
	if rangeCheck == nil and RangeCheck[SavedVars.class] ~= nil and RangeCheck[SavedVars.class][SavedVars.spec] ~= nil then
		rangeCheck = RangeCheck[SavedVars.class][SavedVars.spec]
	end

	-- need check
	if rangeCheck ~= nil then
		local minRange, maxRange = rc:GetRange('target')
		if (maxRange or 0) > rangeCheck then
			return false
		end
	end
	
    -- pet heal
    if spell.PetPct ~= nil then
        local pct = UnitHealth("pet") / UnitHealthMax("pet")
        if PetHasBuff(spell.Id, 0) or pct > spell.PetPct then
            return false
        end
    end

    -- kick
    if spell.IsKick ~= nil and spell.IsKick then
        if not IsInterruptable() then
            return false
        end
    end
	
	-- no overlap
    if spell.NoOverlap or false then
        if HasBuff(spell.Id, 0, 0) then
			return false
		end
    end
	
	-- debuff overlap
	if spell.NoOverlapTarget ~= nil then
		if TargetDebuff(spell.Name, true, 0, spell.NoOverlapTarget) then
			return false
		end
	end
	
	-- on target debuff
	if spell.OnTargetDebuff ~= nil then
		local can = false
		for k, v in pairs(spell.OnTargetDebuff) do
            if TargetDebuff(v.Name, true, v.Stack or 0, v.Remain or 1) then
				can = true
				break
			end
		end
		
		if not can then
			return false
		end
	end
	
    -- buff check
    if spell.On ~= nil then
		local can = false
        for k, v in pairs(spell.On) do
            if HasBuff(v.Name, (v.Stack or 0), v.Remain or 0) then
                can = true
            end
        end
		
		if not can then
			return false
		end
    end
	
	-- stop if have buff
	if spell.Off ~= nil then
        for k, v in pairs(spell.Off) do
            if HasBuff(v.Name, (v.Stack or 0), v.Remain or 0) then
                return false
            end
        end
    end
	
	-- stop if other not on cd
	if spell.Cooldown ~= nil then
        for k, v in pairs(spell.Cooldown) do
            if not SpellCooldown(v.Id) then
                return false
            end
        end
    end

    -- send keys and update UI
    SendKey(spell.Name)
    return true
end

function SendKey(spellName)
	local mod = GetSpellModKey(spellName)
	local key = GetSpellNumKey(spellName)
	
    SetKey(mod, key)
    SetStatusText(spellName)
end