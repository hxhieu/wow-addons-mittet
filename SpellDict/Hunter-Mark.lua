SpellDict["HUNTER"]["Marksmanship"] =
{
    -- Kicks
    {
        Name = "Counter Shot",
        Id = 147362,
        IsKick = true
    },

    -- Sequences in order
	{
        Name = "Barrage",
        Id = 120360,
        FireMode = 2,
        Power = 20
    },
	{
		Name = "Aimed Shot",
        Id = 19434,
        FireMode = 2,
		On = 
		{
			{ Name = "Lock and Load" }
		}
    },
	{
        Name = "Windburst",
        Id = 204147,
        FireMode = 2,
        Power = 20
    },
    {
        Name = "Aimed Shot",
        Id = 19434,
        FireMode = 0,
        Power = 50,
        OnTargetDebuff =
        {
            { Name = "Vulnerable", Remain = 3 }
        }
    },
    {
        Name = "Marked Shot",
        Id = 185901,
        FireMode = 2,
		Power = 30,
		OnTargetDebuff =
        {
            { Name = "Hunter's Mark" }
        }
    },
	{
        Name = "Sidewinders",
        Id = 241579,
        FireMode = 2,
        On =
        {
            { Name = "Marking Targets" }
        }
    },
	{
		Name = "Aimed Shot",
        Id = 19434,
        FireMode = 0,
        Power = 50
    },
	{
        Name = "Sidewinders",
        Id = 241579,
        FireMode = 2
    }
}