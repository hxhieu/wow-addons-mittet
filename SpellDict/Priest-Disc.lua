SpellDict["PRIEST"]["Discipline"] =
{
    -- Kicks
    
    -- Sequences in order
	{
        Name = "Shadow Word: Pain",
        Id = 589,
        FireMode = 2,
		NoOverlapTarget = 2
    },
	{
        Name = "Penance",
        Id = 47540,
        FireMode = 2
    },
    {
        Name = "Smite",
        Id = 585,
        FireMode = 2,
		OnTargetDebuff = { 
			{ Name = "Shadow Word: Pain" }
		}
    }
}