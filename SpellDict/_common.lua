SpellDict = {
    ["HUNTER"] = {},
    ["PALADIN"] = {},
    ["DRUID"] = {},
	["WARRIOR"] = {},
	["PRIEST"] = {},
	["SHAMAN"] = {}
}

RangeCheck = {
    ["HUNTER"] =
    {
        ["Beast Mastery"] = 40,
        ["Marksmanship"] = 45
    },
    ["PALADIN"] =
    {
        ["Protection"] = 5,
        ["Retribution"] = 5,
		["Holy"] = 5
    },
	["WARRIOR"] =
    {
        ["Protection"] = 5,
        ["Arms"] = 5
    },
	["PRIEST"] =
    {
        ["Holy"] = 40,
        ["Discipline"] = 40,
		["Shadow"] = 40
    },
	["SHAMAN"] =
    {
        ["Enhancement"] = 10,
        ["Elemental"] = 40,
		["Restoration"] = 40
    }
}