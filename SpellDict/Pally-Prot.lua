SpellDict["PALADIN"]["Protection"] =
{
	--{
        --Name = "Light of the Protector",
		--Id = 184092,
        --FireMode = 2,
		--Range = 40,
        --MaxHP = 0.9
    --},
	{
        Name = "Hand of the Protector",
        Id = 213652,
        FireMode = 2,
		Range = 40,
        MaxHP = 0.9
    },
    {
        Name = "Guardian of Ancient Kings",
        Id = 86659,
        FireMode = 2,
        MaxHP = 0.5
    },
    {
        Name = "Lay on Hands",
        Id = 633,
        FireMode = 2,
        MaxHP = 0.2
    },
	
    -- Kicks
    {
        Name = "Rebuke",
        Id = 96231,
        IsKick = true
    },

    -- buff
    
    -- Sequences in order
    {
        Name = "Consecration",
        Id = 26573,
        FireMode = 2
    },
    {
        Name = "Avenger's Shield",
        Id = 31935,
        FireMode = 2,
        Range = 30
    },
	{
        Name = "Eye of Tyr",
        Id = 209202,
        FireMode = 2,
		MaxHP = 0.9
    },
    {
        Name = "Judgment",
        Id = 20271,
        FireMode = 2,
        IsOpening = true,
        Range = 30
    },
    {
        Name = "Hammer of the Righteous",
        Id = 53595,
        FireMode = 2
    },
    {
        Name = "Shield of the Righteous",
        Id = 53600,
        FireMode = 2,
		NoOverlap = true
    }
}