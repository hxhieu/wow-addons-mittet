SpellDict["HUNTER"]["Beast Mastery"] =
{
    -- Kicks
    {
        Name = "Counter Shot",
        Id = 147362,
        IsKick = true
    },

    -- Buffs for config, should not be in sequences
    {
        Name = "Steady Focus",
        Id = 177667,
        IsBuff = true
    },
    {
        Name = "Frenzy",
        Id = 19623,
        IsBuff = true
    },


    -- Sequences in order
    {
        Name = "Bestial Wrath",
        Id = 19574,
        IsOpening = true,
        FireMode = 2,
        Power = 100
    },
    {
        Name = "Focus Fire",
        Id = 82692,
        FireMode = 2,
        NoOverlap = true,
        On1 =
        {
            ["Frenzy"] = { Id = 19623, Stack = 3 }
        }
    },
    {
        Name = "Mend Pet",
        Id = 136,
        PetPct = 0.7,
        FireMode = 2
    },
    {
        Name = "Kill Command",
        Id = 34026,
        IsOpening = true,
        FireMode = 2,
        Power = 40,
        On1 =
        {
            ["Steady Focus"] = { Id = 177667 }
        },
        On2 =
        {
            ["Bestial Wrath"] = { Id = 19574 }
        },
        OnCooldown =
        {
            ["Bestial Wrath"] = { Id = 19574 }
        }
    },
    {
        Name = "Kill Shot",
        Id = 53351,
        FireMode = 2,
        Finish = 0.2
    },
    {
        Name = "Barrage",
        Id = 120360,
        FireMode = 2,
        Power = 60,
        On1 =
        {
            ["Steady Focus"] = { Id = 177667 }
        },
        On2 =
        {
            ["Bestial Wrath"] = { Id = 19574 }
        },
        OnCooldown =
        {
            ["Bestial Wrath"] = { Id = 19574 }
        }
    },
    {
        Name = "Multi-Shot",
        Id = 2643,
        IsOpening = true,
        FireMode = 1,
        Power = 40,
        On1 =
        {
            ["Steady Focus"] = { Id = 177667 }
        },
        On2 =
        {
            ["Bestial Wrath"] = { Id = 19574 }
        },
        OffPet =
        {
            ["Beast Cleave"] = { Id = 115939 }
        }
    },
    {
        Name = "Arcane Shot",
        Id = 3044,
        Power = 60,
        On1 =
        {
            ["Steady Focus"] = { Id = 177667 }
        },
        OnCooldown =
        {
            ["Bestial Wrath"] = { Id = 19574 }
        }
    },
    {
        Name = "Arcane Shot",
        Id = 3044,
        Power = 30,
        On1 =
        {
            ["Bestial Wrath"] = { Id = 19574 }
        },
        OnCooldown =
        {
            ["Bestial Wrath"] = { Id = 19574 }
        }
    },
    {
        Name = "Cobra Shot",
        Id = 77767,
        FireMode = 2,
        Off =
        {
            ["Bestial Wrath"] = { Id = 19574 }
        }
    }
}