SpellDict["SHAMAN"]["Enhancement"] =
{
	-- Kicks
    {
        Name = "Wind Shear",
        Id = 57994,
		Range = 30,
        IsKick = true
    },
	
	{
        Name = "Astral Shift",
        Id = 108271,
		MaxHP = 0.25,
        FireMode = 2
    },
	
    -- Sequences in order
	{
        Name = "Crash Lightning",
        Id = 187874,
        Power = 20,
        FireMode = 1
    },
	{
        Name = "Frostbrand",
        Id = 196834,
		Power = 20,
        FireMode = 0,
		Off =
        {
            { Name = "Frostbrand", Remain = 2 }
        }
    },
	{
        Name = "Flametongue",
        Id = 193796,
        FireMode = 0,
		Off =
        {
            { Name = "Flametongue", Remain = 2 }
        }
    },
	{
        Name = "Stormstrike",
        Id = 17364,
        Power = 20,
        FireMode = 2,
		On = 
		{
			{ Name = "Stormbringer" }
		}
    },
	{
        Name = "Stormstrike",
        Id = 17364,
        Power = 40,
        FireMode = 2
    },
	{
        Name = "Lava Lash",
        Id = 60103,
        Power = 45,
		Cooldown =
        {
            { Id = "17364" }
        },
        FireMode = 2
    },
    {
        Name = "Rockbiter",
        Id = 193786,
        FireMode = 2
    }
}