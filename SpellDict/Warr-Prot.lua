SpellDict["WARRIOR"]["Protection"] =
{

    -- Kicks
    
    -- buff
	{
        Name = "Victorious",
        Id = 32216,
        FireMode = 2,
		IsBuff = true
    },
    
    -- Sequences in order
    {
        Name = "Shield Slam",
        Id = 23922,
        FireMode = 2
    },
	{
        Name = "Thunder Clap",
        Id = 6343,
        FireMode = 2
    },
    {
        Name = "Victory Rush",
        Id = 34428,
        FireMode = 2,
		MaxHP = 0.7,
		On1 = {
            ["Victorious"] = { Id = 32216 }      
	  }
    },
	{
        Name = "Shield Block",
        Id = 2565,
        FireMode = 2,
		Power = 70,
		NoOverlap = true
    },
	{
        Name = "Devastate",
        Id = 20243,
        FireMode = 2
    }
}