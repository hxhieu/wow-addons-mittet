SpellDict["PALADIN"]["Holy"] =
{
    {
        Name = "Lay on Hands",
        Id = 633,
        FireMode = 2,
        MaxHP = 0.2
    },
	
    -- Kicks

    -- buff
    
    -- Sequences in order
    {
        Name = "Holy Shock",
        Id = 20473,
        FireMode = 2,
		Range = 40
    },
    {
        Name = "Judgment",
        Id = 20271,
        FireMode = 2,
        IsOpening = true,
        Range = 30
    },
	{
        Name = "Consecration",
        Id = 26573,
        FireMode = 2
    },
    {
        Name = "Crusader Strike",
        Id = 35395,
        FireMode = 2
    }
}