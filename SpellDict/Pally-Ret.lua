SpellDict["PALADIN"]["Retribution"] =
{

    -- Kicks
    {
        Name = "Rebuke",
        Id = 96231,
        IsKick = true
    },
    -- {
        -- Name = "Arcane Torrent",
        -- Id = 28730,
        -- IsKick = true
    -- },
    
    -- Sequences in order
--	{
--        Name = "Hammer of Justice",
--        Id = 853,
--        PowerPoint = 5,
--        FireMode = 2
--    },
--    {
--        Name = "Justicar's Vengeance",
--        Id = 215661,
--        PowerPoint = 5,
--        FireMode = 2
--    },
--	{
--        Name = "Justicar's Vengeance",
--        Id = 215661,
--        FireMode = 2,
--		On =
--        {
--            { Name = "Divine Purpose" }
--        }
--  },
	{
        Name = "Judgment",
        Id = 20271,
        FireMode = 2,
        IsOpening = true,
        Range = 30
    },
	{
        Name = "Wake of Ashes",
        Id = 205273,
        FireMode = 2
    },
    {
        Name = "Templar's Verdict",
        Id = 85256,
        PowerPoint = 3,
        FireMode = 0
    },
    {
        Name = "Templar's Verdict",
        Id = 85256,
        FireMode = 0,
        On =
        {
            { Name = "Divine Purpose" }
        }
    },
    {
        Name = "Divine Storm",
        Id = 53385,
        PowerPoint = 3,
        FireMode = 1,
		Range = 10
    },
    {
        Name = "Divine Storm",
        Id = 53385,
        FireMode = 1,
		Range = 10,
        On =
        {
            { Name = "Divine Purpose" }
        }
    },
	{
        Name = "Blade of Justice",
        Id = 184575,
		Range = 10,
        FireMode = 2
    },
    {
        Name = "Crusader Strike",
        Id = 35395,
        FireMode = 2
    }
}