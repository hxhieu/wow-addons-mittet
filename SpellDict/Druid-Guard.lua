SpellDict["DRUID"]["Guardian"] =
{
    -- Kicks
    {
        Name = "Skull Bash",
        Id = 106839,
        IsKick = true
    },

    -- Sequences in order
    {
        Name = "Chimaera Shot",
        Id = 53209,
        IsOpening = true,
        FireMode = 2,
        Power = 35
    },
    {
        Name = "Kill Shot",
        Id = 53351,
        FireMode = 2,
        Finish = 0.35
    },
    {
        Name = "Barrage",
        Id = 120360,
        FireMode = 2,
        Power = 60,
        OnCooldown =
        {
            ["Chimaera Shot"] = { Id = 53209 }
        }
    },
    {
        Name = "Aimed Shot",
        Id = 19434,
        Power = 50,
        Off =
        {
            ["Thrill of the Hunt"] = { Id = 34720 }
        },
        OnCooldown =
        {
            ["Chimaera Shot"] = { Id = 53209 },
            ["Barrage"] = { Id = 120360 }
        }
    },
    {
        Name = "Aimed Shot",
        Id = 19434,
        Power = 30,
        On =
        {
            ["Thrill of the Hunt"] = { Id = 34720 }
        },
        OnCooldown =
        {
            ["Chimaera Shot"] = { Id = 53209 },
            ["Barrage"] = { Id = 120360 }
        }
    },
    {
        Name = "Multi-Shot",
        Id = 2643,
        FireMode = 1,
        Power = 40,
        Off =
        {
            ["Thrill of the Hunt"] = { Id = 34720 }
        },
        OnCooldown =
        {
            ["Chimaera Shot"] = { Id = 53209 },
            ["Barrage"] = { Id = 120360 }
        }
    },
    {
        Name = "Multi-Shot",
        Id = 2643,
        FireMode = 1,
        Power = 20,
        On =
        {
            ["Thrill of the Hunt"] = { Id = 34720 }
        },
        OnCooldown =
        {
            ["Chimaera Shot"] = { Id = 53209 },
            ["Barrage"] = { Id = 120360 }
        }
    },
    {
        Name = "Steady Shot",
        Id = 56641,
        FireMode = 2
    }
}