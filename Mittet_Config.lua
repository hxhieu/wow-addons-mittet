MittetConfig = { }

local AceGUI = LibStub("AceGUI-3.0")
local configFrame = nil

local frameSize = {
    Width = 750,
    Height = 510
}

local function ShowConfigFrame()
    -- create an ACE frame
    if configFrame ~= nil then
        configFrame:Release()
        configFrame = nil
    end
    if SavedVars.Configuration == nil then SavedVars.Configuration = { } end
    if SavedVars.Configuration[SavedVars.spec] == nil then SavedVars.Configuration[SavedVars.spec] = { } end

    configFrame = AceGUI:Create("Window")
    configFrame:SetTitle("Mittet Configuration")
    configFrame:SetLayout("List")
    configFrame:EnableResize(false)
    configFrame:SetWidth(frameSize.Width)
    configFrame:SetHeight(frameSize.Height)
    configFrame:ReleaseChildren()

    local lblClass = AceGUI:Create("Heading")
    lblClass:SetFullWidth(true)
    lblClass:SetText(SavedVars.class .. " / " .. SavedVars.spec)

    local lblError = AceGUI:Create("Label")
    lblError:SetFullWidth(true)
    lblError:SetColor(1, 0, 0, 1)

        local iconSpell = AceGUI:Create("Icon")
        iconSpell:SetImageSize(40, 40)

        local editbox = AceGUI:Create("EditBox")
        editbox:SetLabel("Spell Name:")
        editbox:SetWidth(200)


        editbox:SetFocus()
        editbox:SetCallback("OnEnterPressed", function(self, event, text, ...)
            local name, rank, icon, castingTime, minRange, maxRange, spellID = GetSpellInfo(text)

            print("--------")
            print(name)
            print(spellID)

            iconSpell:SetImage(icon)
            iconSpell:SetLabel(name)
        end )

        configFrame:AddChild(editbox)
        configFrame:AddChild(iconSpell)

    configFrame:AddChild(lblClass)
    configFrame:AddChild(lblError)

    -- load the dict for this class/spec
    local classSpell = SpellDict[SavedVars.class]
    if classSpell == nil then
        lblError:SetText("Sorry, your class is not yet implemented.")
        return
    end
    local specSpell = classSpell[SavedVars.spec]
	
    if specSpell == nil then
        lblError:SetText("Sorry, your spec is not yet implemented.")
        return
    end

    -- spell table
    local tblSpell = AceGUI:Create("InlineGroup")
    tblSpell:SetLayout("Flow")
    tblSpell:SetTitle("Spell Hotkeys")
    tblSpell:SetFullWidth(true)

    local addedSpell = { }

    for k, v in pairs(specSpell) do
        if not v.IsBuff then
            local name, rank, icon, castingTime, minRange, maxRange, spellID = GetSpellInfo(v.Name)
            if spellID ~= nil and addedSpell[name] == nil then
                local group = CreateSpellConfigGroup(name, icon, 100)
                tblSpell:AddChild(group)
                addedSpell[name] = name
            end
        end
    end

    configFrame:AddChild(tblSpell)
end

function CreateSpellConfigGroup(name, icon, width)

    local savedConfig = SavedVars.Configuration[SavedVars.spec][name]
    if savedConfig == nil then
        savedConfig = { key = 0, mod = 0, active = false }
        SavedVars.Configuration[SavedVars.spec][name] = savedConfig
    end

    local group = AceGUI:Create("SimpleGroup")
    group:SetLayout("List")
    group:SetWidth(width)

    local iconSpell = AceGUI:Create("Icon")
    iconSpell:SetImageSize(width / 3, width / 3)
    iconSpell:SetImage(icon)
    iconSpell:SetLabel(name)
    iconSpell:SetFullWidth(true)

    local ddlMod = AceGUI:Create("Dropdown")
    ddlMod:SetFullWidth(true)
    ddlMod:SetList( { "-", "ALT", "CTRL", "SHIFT" })
    ddlMod:SetValue(savedConfig.mod + 1)
    ddlMod:SetLabel("Modifier")
    ddlMod:SetCallback("OnValueChanged", function(self, event, value)
        savedConfig.mod = value - 1
        savedConfig.active = savedConfig.mod > 0 and savedConfig.key > 0
        SavedVars.Configuration[SavedVars.spec][name] = savedConfig
    end )

    local ddlKey = AceGUI:Create("Dropdown")
    ddlKey:SetFullWidth(true)
    ddlKey:SetList( { "-", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12" })
    ddlKey:SetValue(savedConfig.key + 1)
    ddlKey:SetLabel("Key")
    ddlKey:SetCallback("OnValueChanged", function(self, event, value)
        savedConfig.key = value - 1
        savedConfig.active = savedConfig.mod > 0 and savedConfig.key > 0
        SavedVars.Configuration[SavedVars.spec][name] = savedConfig
    end )

    group:AddChild(iconSpell)
    group:AddChild(ddlMod)
    group:AddChild(ddlKey)

    return group
end

function CreateBuffConfigGroup(name, icon, width)

    local savedConfig = SavedVars.Configuration[SavedVars.spec][name]
    if savedConfig == nil then
        savedConfig = { key = 0, mod = 0, active = false }
        SavedVars.Configuration[SavedVars.spec][name] = savedConfig
    end

    local group = AceGUI:Create("SimpleGroup")
    group:SetLayout("List")
    group:SetWidth(width)

    local iconSpell = AceGUI:Create("Icon")
    iconSpell:SetImageSize(width / 3, width / 3)
    iconSpell:SetImage(icon)
    iconSpell:SetLabel(name)
    iconSpell:SetFullWidth(true)

    local chkUse = AceGUI:Create("CheckBox")
    chkUse:SetLabel("Active")
    chkUse:SetValue(savedConfig.active)
    chkUse:SetCallback("OnValueChanged", function(self, event, value)
        SavedVars.Configuration[SavedVars.spec][name].active = value
    end )

    group:AddChild(iconSpell)
    group:AddChild(chkUse)

    return group
end

local function HideConfigFrame()
    if configFrame ~= nill then
        configFrame:Release()
        configFrame = nil
    end
end

MittetConfig.ShowConfigFrame = ShowConfigFrame
MittetConfig.HideConfigFrame = HideConfigFrame